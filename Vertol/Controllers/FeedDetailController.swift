//
//  FeedDetailController.swift
//  Vertol
//
//  Created by Дмитрий Жданов on 12/02/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

class FeedDetailController: UIViewController {
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textLabel: UILabel!
    var item: FeedItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        logoImageView.layer.cornerRadius = 25
        logoImageView.image = #imageLiteral(resourceName: "Logo")
        DispatchQueue.global(qos: .userInteractive).async {
            let data = API.shared.getImage(imageUrl: self.item.img)
            let image = UIImage(data: data)
            DispatchQueue.main.async {
                self.imageView.image = image
            }
        }
        titleLabel.text = item.title
        dateLabel.text = item.date
        textLabel.text = item.text
    }
}
