//
//  MoreController.swift
//  Vertol
//
//  Created by Дмитрий Жданов on 12/02/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

class MoreController: UITableViewController {
    let items = ["ФСК Стрела", "СДК РостВертол", "Молодежь РостВертола"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoreCell", for: indexPath)
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.text = items[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = getMoreDetailVC()
        VC.name = items[indexPath.row]
        switch items[indexPath.row] {
        case "ФСК Стрела":
            VC.category = "FSK"
        case "СДК РостВертол":
            VC.category = "SDK"
        case "Молодежь РостВертола":
            VC.category = "RY"
        default:
            break
        }
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
