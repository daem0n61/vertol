//
//  PopFilterMenuController.swift
//  Vertol
//
//  Created by Дмитрий Жданов on 11/02/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

class PopFilterMenuController: UITableViewController {
    let items = ["Все новости", "ФСК Стрела", "СДК РостВертол", "Молодежь"]
    var didSelectItem: ((_ item: String) -> Void)!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PopMenuCell", for: indexPath)
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.text = items[indexPath.row]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.didSelectItem(items[indexPath.row])
        self.dismiss(animated: true)
    }
}
