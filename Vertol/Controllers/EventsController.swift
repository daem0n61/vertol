//
//  EventsController.swift
//  Vertol
//
//  Created by Дмитрий Жданов on 12/02/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

class EventsController: UITableViewController {
    var events = [FeedItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 250
        tableView.rowHeight = UITableView.automaticDimension
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshHandler), for: .valueChanged)
        refreshHandler()
    }
    
    @objc func refreshHandler() {
        DispatchQueue.global(qos: .userInteractive).async {
            let data = API.shared.getEvents()
            do {
                let parsed = try JSONDecoder().decode(FeedAnswer.self, from: data)
                if parsed.answer == "error" {
                    presentAlert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
                    return
                }
                self.events.removeAll()
                self.events = parsed.result!
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.5) {
                        self.refreshControl?.endRefreshing()
                    }
                    self.tableView.reloadData()
                }
            } catch {
                presentAlert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
                return
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return events.count > 0 ? events.count : 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if events.count > 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell", for: indexPath) as? FeedCell {
                cell.fill(item: events[indexPath.section])
                return cell
            }
            return UITableViewCell()
        } else {
            let cell = UITableViewCell()
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.text = "Нет событий"
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return events.count > 0 ? 250 : 50
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = getFeedDetailVC()
        VC.item = events[indexPath.section]
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
