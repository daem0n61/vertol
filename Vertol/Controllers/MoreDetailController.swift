//
//  MoreDetailController.swift
//  Vertol
//
//  Created by Дмитрий Жданов on 12/02/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

class MoreDetailController: UIViewController {
    @IBOutlet weak var textLabel: UILabel!
    var category, name: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = name
        DispatchQueue.global(qos: .userInteractive).async {
            let data = API.shared.getStatic(category: self.category)
            do {
                let parsed = try JSONDecoder().decode(StaticAnswer.self, from: data)
                if parsed.answer == "error" {
                    presentAlert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
                    return
                }
                
                DispatchQueue.main.async {
                    self.textLabel.text = parsed.result!
                }
            } catch {
                presentAlert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
                return
            }
        }
    }
}
