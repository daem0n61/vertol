//
//  NewsController.swift
//  Vertol
//
//  Created by Дмитрий Жданов on 11/02/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

class NewsController: UITableViewController {
    @IBOutlet weak var filterButton: UIButton!
    var news = [FeedItem]()
    var category = "all"
    let picker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 250
        tableView.rowHeight = UITableView.automaticDimension
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshHandler), for: .valueChanged)
        refreshHandler()
    }
    
    @objc func refreshHandler() {
        DispatchQueue.global(qos: .userInteractive).async {
            let data = API.shared.getNews(category: self.category, month: "null")
            do {
                let parsed = try JSONDecoder().decode(FeedAnswer.self, from: data)
                if parsed.answer == "error" {
                    presentAlert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
                    return
                }
                self.news.removeAll()
                self.news = parsed.result!
                DispatchQueue.main.async {
                    UIView.animate(withDuration: 0.5) {
                        self.refreshControl?.endRefreshing()
                    }
                    self.tableView.reloadData()
                }
            } catch {
                presentAlert(VC: self, message: "Что-то пошло не так. Проверьте соединение с интернетом или попробуйте позже")
                return
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return news.count > 0 ? news.count : 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if news.count > 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "NewsCell", for: indexPath) as? FeedCell {
                cell.fill(item: news[indexPath.section])
                return cell
            }
            return UITableViewCell()
        } else {
            let cell = UITableViewCell()
            cell.textLabel?.textAlignment = .center
            cell.textLabel?.text = "Нет новостей по заданному фильтру"
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return news.count > 0 ? 250 : 50
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = getFeedDetailVC()
        VC.item = news[indexPath.section]
        self.navigationController?.pushViewController(VC, animated: true)
    }

    @IBAction func filterButtonTapped(_ sender: Any) {
        let popVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopFilterMenuVC") as! PopFilterMenuController
        popVC.modalPresentationStyle = .popover
        let popOverVC = popVC.popoverPresentationController
        popOverVC?.delegate = self
        popOverVC?.sourceView = self.filterButton
        popOverVC?.sourceRect = CGRect(x: self.filterButton.bounds.midX, y: self.filterButton.bounds.maxY, width: 0, height: 0)
        popVC.preferredContentSize = CGSize(width: self.view.frame.maxX * 2/3, height: 175)
        popVC.didSelectItem = { (item) in
            self.filterButton.setTitle(item, for: .normal)
            switch item {
            case "Все новости":
                self.category = "all"
            case "ФСК Стрела":
                self.category = "FSK"
            case "СДК РостВертол":
                self.category = "SDK"
            case "Молодежь":
                self.category = "RY"
            default:
                break
            }
            self.refreshHandler()
        }
        self.present(popVC, animated: true)
    }
    
    @IBAction func calendarButtonTapped(_ sender: Any) {
        let VC = getCalendarVC()
        /*VC.didSelectItem = { (item) in
            switch item {
            case "Январь":
                
            case "Февраль":
                
            case "Март":
                
            case "Апрель":
                
            case "Май":
                
            case "Июнь":
                
            case "Июль":
                
            case "Август":
                
            case "Сентябрь":
                
            case "Октябрь":
                
            case "Ноябрь":
                
            case "Декабрь":
                
            default:
                <#code#>
            }
        }*/
        self.navigationController?.pushViewController(VC, animated: true)
    }
}

extension NewsController: UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}
