//
//  MonthCell.swift
//  Vertol
//
//  Created by Дмитрий Жданов on 13/02/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

class MonthCell: UICollectionViewCell {
    @IBOutlet weak var title: UILabel!
    
    func fill(title: String) {
        self.title.text = title
    }
}
