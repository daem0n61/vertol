//
//  NewsCell.swift
//  Vertol
//
//  Created by Дмитрий Жданов on 12/02/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell {
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var mainImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func fill(item: FeedItem) {
        logoImageView.layer.cornerRadius = 25
        logoImageView.image = #imageLiteral(resourceName: "Logo")
        DispatchQueue.global(qos: .userInteractive).async {
            let data = API.shared.getImage(imageUrl: item.img)
            let image = UIImage(data: data)
            DispatchQueue.main.async {
                self.mainImageView.image = image
            }
        }
        titleLabel.text = item.title
        dateLabel.text = item.date
    }
}
