//
//  API.swift
//  Vertol
//
//  Created by Дмитрий Жданов on 12/02/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import Foundation

enum MethodType {
    case post
    case get
}

class API {
    static let shared = API()
    private var data: Data!
    private let baseURL = "http://46.229.215.224:9999/api"
    
    func getNews(category: String, month: String) -> Data {
        configRequest(url: "GetNews?category=\(category)&month=\(month)", type: .get, token: nil, params: nil)
        return data
    }
    
    func getImage(imageUrl: String) -> Data {
        configRequest(url: imageUrl, type: .get, token: nil, params: nil)
        return data
    }
    
    func getDiscounts() -> Data {
        configRequest(url: "GetDiscounts", type: .get, token: nil, params: nil)
        return data
    }
    
    func getEvents() -> Data {
        configRequest(url: "GetEvents", type: .get, token: nil, params: nil)
        return data
    }
    
    func getStatic(category: String) -> Data {
        configRequest(url: "GetStatic?category=\(category)", type: .get, token: nil, params: nil)
        return data
    }
    
    fileprivate func configRequest(url: String, type: MethodType, token: String?, params: [String: Any]?) {
        guard let url = URL(string: "\(baseURL)/\(url)") else {return}
        var request = URLRequest(url: url)
        
        switch type {
        case .get:
            request.httpMethod = "Get"
        case .post:
            request.httpMethod = "Post"
        }
        
        if let token = token {
            request.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        }
        if let params = params {
            request.addValue("application/json", forHTTPHeaderField: "Content-type")
            let httpBody = try! JSONSerialization.data(withJSONObject: params, options:[])
            request.httpBody = httpBody
        }
        
        sendRequest(request: request)
    }
    
    fileprivate func sendRequest(request: URLRequest) {
        let semaphore = DispatchSemaphore(value: 0)
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            //if let response = response {print(response)}
            self.data = data
            semaphore.signal()
            }.resume()
        semaphore.wait()
    }
}
