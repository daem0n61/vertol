//
//  Utils.swift
//  Vertol
//
//  Created by Дмитрий Жданов on 12/02/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import UIKit

func printJSON(data: Data) {
    print(try! JSONSerialization.jsonObject(with: data, options: []))
}

func presentAlert(VC: UIViewController, message: String) {
    let alertVC = UIAlertController(title: nil, message: message, preferredStyle: .alert)
    let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertVC.addAction(alertAction)
    VC.present(alertVC, animated: true, completion: nil)
}

func getFeedDetailVC() -> FeedDetailController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FeedDetailVC") as! FeedDetailController
}

func getMoreDetailVC() -> MoreDetailController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MoreDetailVC") as! MoreDetailController
}

func getCalendarVC() -> CalendarController {
    return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CalendarVC") as! CalendarController
}
