//
//  Model.swift
//  Vertol
//
//  Created by Дмитрий Жданов on 12/02/2019.
//  Copyright © 2019 Дмитрий Жданов. All rights reserved.
//

import Foundation

struct FeedItem: Codable {
    let title, text, img, date: String
}

struct FeedAnswer: Codable {
    let answer: String
    let result: [FeedItem]?
}

struct PashinKostbll: Codable {
    let text: String
}

struct StaticAnswer: Codable {
    let answer: String
    let result: String?
}
